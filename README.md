## Hyve React Test Project

### Goal

Create a React application that uses the Reddit API to pull a subreddit, and displays each image of the subreddit.
* eslint to be adhered to. 

* Redux to be used for state management. 

* Handle all potential errors that may occur in a graceful manner 

* Maintain a clean codebase.
