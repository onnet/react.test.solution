import { applyMiddleware, createStore } from 'redux';
import promise from 'redux-promise-middleware';
import thunk from 'redux-thunk';

// Reducers
import reducer from './reducers';

let middleware = applyMiddleware(promise(), thunk);

export default createStore(reducer, middleware);