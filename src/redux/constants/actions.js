const homeActions = {
    getPostsFail: 'GET_POSTS_FAIL',
    getPostsLoad: 'GET_POSTS_LOAD',
    getPostsSuccess: 'GET_POSTS_SUCCESS'
};

export default homeActions;
