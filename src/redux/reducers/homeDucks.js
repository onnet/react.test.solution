import axios from 'axios';
import homeActions from '../constants/actions';

const initialState = {
    // Array
    postData: [],

    // Boolean
    load: true
};

const getPostsUrl = 'https://www.reddit.com/r/aww.json';

// Reducer
export default function reducer(state = initialState, action) {
    switch (action.type) {
        case homeActions.getPostsFail: {
            return state;
        }

        case homeActions.getPostsLoad: {
            return {
                ...state,
                load: true
            };
        }

        case homeActions.getPostsSuccess: {
            return {
                ...state,
                load: false,
                postData: action.payload
            };
        }

        default: {
            return state;
        }
    }
}

// Actions
export function getPosts() {
    return (dispatch) => {
        dispatch({
            type: homeActions.getPostsLoad,
            payload: ''
        });

        axios.get(getPostsUrl)
            .then((response) => {
                const data = response.data.data.children;
                const transformedData = [];

                for (let x = 0; x < data.length; x++) {
                    const obj = {
                        url: ''
                    };

                    transformedData[x] = obj;
                    transformedData[x].url = data[x].data.url;
                }

                dispatch({
                    type: homeActions.getPostsSuccess,
                    payload: transformedData
                });
            })
            .catch((error) => {
                // handle error
                console.log(error);
                dispatch({
                    type: homeActions.getPostsFail,
                    payload: ''
                });
            });
    };
}
