import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

// Bootstrap components
import { Carousel, ProgressBar } from 'react-bootstrap';

import {
    getPosts
} from '../../redux/reducers/homeDucks';

// Styles
require('./styles.scss');

const defaultProps = {
    // Arrays
    postData: [],

    // Boolean
    load: true,

    // Objects
    actions: PropTypes.object
};

const propTypes = {
    // Arrays
    postData: PropTypes.instanceof(Array),

    // Boolean
    load: PropTypes.bool,

    // Objects
    actions: {
        getPosts: PropTypes.func
    }
};

class Home extends Component {
    static handleError(event) {
        const ele = event.currentTarget;
        ele.src = 'notFound.png';
    }

    renderItems(props) {
        const { postData } = props.postData;
        const items = postData.map((item, key) => {
            return (
                <Carousel.Item key={item.url}>
                    <div className="home__item">
                        <img
                            src={item.url}
                            onError={this.constructor.handleError}
                            data-id={key}
                            alt="Reddit"
                        />
                    </div>
                </Carousel.Item>
            );
        });

        return items;
    }

    renderLoad(props) {
        const { load } = props.load;
        if (load) {
            return (
                <div className="home">
                    <div className="home__load">
                        <ProgressBar
                            now={100}
                            active
                        />
                    </div>
                </div>
            );
        }
        return (
            <div className="home">
                <div className="home__sliderWrapper">
                    <Carousel
                        interval={null}
                    >
                        {this.renderItems()}
                    </Carousel>
                </div>
            </div>
        );
    }

    componentDidMount(props) {
        const { getPosts } = props.actions.getPosts;
        getPosts();
    }

    render() {
        return (
            <div>
                {this.renderLoad()}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    load: state.homeReducer.load,
    postData: state.homeReducer.postData
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        getPosts
    },
    dispatch)
});

Home.defaultProps = defaultProps;
Home.propTypes = propTypes;

export default connect(mapStateToProps, mapDispatchToProps)(Home);
